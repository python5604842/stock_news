import aiohttp
import asyncio


async def fetch_news(theme, from_date, to_date, api_key):
    parameters = {
        "q": theme,
        "from": from_date,
        "to": to_date,
        "sortBy": "popularity",
        "apiKey": api_key
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url="https://newsapi.org/v2/everything", params=parameters) as response:
            data = await response.json()
            return data["articles"]

# Example of an article extraction
# article = data["articles"][0]
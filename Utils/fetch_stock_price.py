import aiohttp
import asyncio


async def fetch_data(symbol, api_key):
    parameters = {
        "function": "TIME_SERIES_DAILY",
        "symbol": symbol,
        "apikey": api_key
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url="https://www.alphavantage.co/query", params=parameters) as r:
            return await r.json()
